
## This template is for requesting minor updates of existing pages.

This repository primarily relates to the logged out [marketing website](https://about.gitlab.com/) and [company handbook](https://about.gitlab.com/handbook/). This is not the right repository for requests related to docs.gitlab.com, product, or other parts of the site.

# Issues should identify [The Five W's](https://en.wikipedia.org/wiki/Five_Ws) : who, what, when, where, and why.

It is also recommended to apply [MoSCoW](https://en.wikipedia.org/wiki/MoSCoW_method) sorting to your requests (must, should, could, won't).

#### Please apply appropriate labels or your issue may not be sorted on to appropriate boards.

Please refer to the [website label documentation](/handbook/marketing/brand-and-digital-design/#issue-labels)

## Please delete the lines above and any irrelevant sections below.

Below are suggestions. Any extra information you provide is beneficial.

#### What is/are the relevant URL(s)

(Example: https://about.gitlab.com/compare/asdf/)

#### Briefly describe the page/flow

(Example: This page compares us to a specific competitor)

#### What is the primary reason for updating this page?

(Example: Increase EE trials)

#### What exactly do you want to change on this page?

(Examples: Add speakers to an event, update hyperlinks, increase button size. Please be specific with details.)

#### What are the KPI (Key Performance Indicators)

(Example: Premium signups per month as identified by Periscope dashboard URL)

#### Will this change require any new or updated events, parameters, or tracking?

(Example: Clicking button X should record a purchase into analytics)

#### Will this change require any new or updated reports?

(Example: This impacts the Retention dashboard in Periscope)

#### Will this change require any new or updated automation?

(Example: Create a test ensuring a job listing is never empty)

#### Will this change require any page redirects?

(Example: redirect tld.com/url/asdf to tld.com/url/zxcv)

<!-- These labels will be automatically applied unless you edit or delete the following section -->
/label ~"mktg-website" ~"template::web-update" ~"mktg-status::triage"

<!-- If you do not want to ping the website team, please remove the following section -->
/cc @gl-website

