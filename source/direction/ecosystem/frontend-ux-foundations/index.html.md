---
layout: markdown_page
title: "Product Direction - Frontend Foundation"
---

- TOC
{:toc}

This is the product vision for Frontend Foundation, and it is currently a work in progress.

## Overview

The FE/UX Foundations category is focused on improving the frontend architecture of GitLab. This includes improving our design system, Pajamas, as well as ensuring that the broader product development organization has the tools required to ensure the GitLab experience is great on mobile.

This category is responsible for our design system [Pajamas](https://design.gitlab.com/) and our frontend architecture: webpack, [PWA](https://developers.google.com/web/progressive-web-apps) needs for improved mobile support, and more.

## Themes

Work in progress.
