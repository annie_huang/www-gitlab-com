---
layout: markdown_page
title: "Category Direction - Release Orchestration"
---

- TOC
{:toc}

## Release Orchestration

Release Orchestration is the ability to coordinate complex releases, particularly
across projects, in an efficient way that leverages as-code behaviors as much as
possible. We also recognize that there are manual steps and coordination points
involving human decisions throughout software delivery in the enterprise. In these cases, there are teams and release manager roles dedicated to groundguiding these complex enterprise releases, rather than individuals continuously deploying to production.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARelease%20Orchestration)
- [Overall Vision](/direction/cicd#release)
- [UX Research](https://gitlab.com/gitlab-org/uxr_insights/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3ARelease%20Orchestration)
- [Documentation](https://docs.gitlab.com/ee/user/project/releases/index.html)

## Manual Processes vs. Automation

At GitLab we believe organizations can accomplish these enterprise grade releases elegantly and effectively with automation. We are a powerful platform that unlocks efficiency within DevOps. As such, we avoid adding manual, non-automated controls to the delivery pipeline.

### Compliance and Security in the Release Pipeline

Security, compliance, control and governance of the release pipeline is handled as part of [Release Governance](/direction/release/release_governance). Secrets in the pipeline are part of our [Secrets Management](/direction/release/secrets_management) category.  Another category for governance and security within GitLab is [Compliance Management](/direction/manage/compliance-management/), which is inclusive of change management and approval workflows. For governance workflows pertaining to issues and requirements, check out our category of [Requirements Management](/direction/plan/requirements_management) from the [Plan](/direction/plan/) stage. 

## What's Next & Why

Our top focus is making release management easier. The two main features that will enable this are release generation from `.gitlab-ci.yml` ([gitlab&2510](https://gitlab.com/groups/gitlab-org/-/epics/2510)) and to create a progress view of all the issues within a Release in ([gitlab#31289](https://gitlab.com/gitlab-org/gitlab/issues/31289)). 

We are also focused on supporting visibility to our user's release plans in the Release page which we will accomplish by expanding the assets of a release to include links to runbooks in [gitlab#9427](https://gitlab.com/gitlab-org/gitlab/issues/9427). 

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).

We believe that expanding editing a release capability directly from the Release page will progress the maturity to Viable. The Releases page will connect the issues assigned to specific milestone and/or release automatically into the workflow, delivering a single view for a release.

Key deliverables to achieve this are:

- [Create draft releases](https://gitlab.com/gitlab-org/gitlab-foss/issues/38105) (Complete)
- [Cross-project environment dashboard](https://gitlab.com/gitlab-org/gitlab/issues/3713) (Complete)
- [Edit Release](https://gitlab.com/gitlab-org/gitlab/issues/26016) (Complete)
- [Association of milestones with releases](https://gitlab.com/gitlab-org/gitlab/issues/29020) (Complete)
- [Filter issues based on release](https://gitlab.com/gitlab-org/gitlab/issues/32632) (Complete)
- [Capture Release actions in the audit log page](https://gitlab.com/gitlab-org/gitlab/issues/32807) (Complete)
- [Auto changelog for release](https://gitlab.com/gitlab-org/gitlab/issues/26015) (12.9)
- [Add link to runbook in release assets](https://gitlab.com/gitlab-org/gitlab/issues/9427) (12.9)
- [Support More actions in the Release Pages UI](https://gitlab.com/groups/gitlab-org/-/epics/2312) (12.10)
- [Release generation from within `.gitlab-ci.yml`](https://gitlab.com/groups/gitlab-org/-/epics/2510) (12.10)


## Competitive Landscape

Release orchestration tools tend to have great features for managing releases,
as you'd expect; they are built from the ground up as a release management
workflow tool and are very capable in those areas. Our approach to release
orchestration will be a bit different, instead of being workflow-oriented we
are going to approach release orchestration from a publishing point of view.
What this means is instead of building complicated workflows for your releases,
we will focus more on the artifact of the release itself and embedding the
checks and steps into it. 

[Gartner listed GitLab as a challenger in Application Release Orchestration](https://about.gitlab.com/blog/2020/01/16/2019-gartner-aro-mq/), 
celebrating the "single product" offering of GitLab as "cloud-native, with a large and rapidly growing customer base". The blurring of the market with the 
convergence of features across various tools in ARO, CI/CS, PaaS, means functionality like AutoDevops and release automation is advantageously positioned to solve 
the problems of more traditional ARO solutions. 

We have conducted a detailed analysis of XebiaLabs' XL Release offering in [gitlab#2369](https://gitlab.com/groups/gitlab-org/-/epics/2369). XL Release's elegance of cross team views, administration of deployments and environments as well as actionable metrics for how Releases are performing are all areas of note. We will be able to compete pointedly by offering [deploy freeze capabilities](https://gitlab.com/gitlab-org/gitlab/issues/24295) and [runbook visibility](https://gitlab.com/gitlab-org/gitlab/issues/9427). Where we will strengthen our approach and confidence in head to head scenarios with XL, will in our next validation tracks. In light of this analysis we aim to improve our experience with environment features like roll backs via [gitlab#198364](https://gitlab.com/gitlab-org/gitlab/issues/198364), blue green deployments with [gitlab#14763](https://gitlab.com/gitlab-org/gitlab/issues/14763) and support a better view at the group-level in with director-level dashboard for Releases in [gitlab#3277](https://gitlab.com/gitlab-org/gitlab/issues/3277). 

Despite the [merger of XebiaLabs by CollabNet](https://www.businesswire.com/news/home/20200121006172/en/CollabNet-VersionOne-XebiaLabs-Combine-Create-Integrated-Agile), GitLab will continue to thrive as a result of the single platform, single data model. This is epecially true as we begin to support more non-technical personas by enhancing the UI experience of release orchestration that many enterprises are interested in. 
 
The next competitive analysis we will conduct will deep dive into [Electric Cloud](https://electric-cloud.com/) in [gitlab#37344](https://gitlab.com/gitlab-org/gitlab/issues/37344). 

## Analyst Landscape

Analysts at this time are looking for more quality of life features that make
a difference in people's daily workflows - how does this make my day better?
By introducing features like [gitlab#26015](https://gitlab.com/gitlab-org/gitlab/issues/26015)
to automatically manage changelogs and release notes as part of releases, we can demonstrate
how our solution is already capable of doing this.

## Top Customer Success/Sales Issue(s)

In the same vein as [gitlab#26015](https://gitlab.com/gitlab-org/gitlab/issues/26015), issues that will support the ease of use of releases like being able to create a release in the UI ([gitlab#32812](https://gitlab.com/gitlab-org/gitlab/issues/32812)) is an issue of interest to satisfy prospect use cases. 

## Top Customer Issue(s)

Implementing [gitlab#26014](https://gitlab.com/gitlab-org/gitlab/issues/26014),
which makes creation of the release package an inline part of the
`.gitlab-ci.yml`, will make this feature feel much more mature and
production-ready (even if it is already really usable.) 

A second top customer request is to auto-generate release notes from annotated tags via [gitlab#15563](https://gitlab.com/gitlab-org/gitlab/issues/15563).

Lastly, we have validated we need to support adding assets to a release ([gitlab#27300](https://gitlab.com/gitlab-org/gitlab/issues/27300)). The connection of releases and assets will improve the richness of the release page, helping users see the full picture of their release in a single place. 

## Top Internal Customer Issue(s)

A lot of interest has been expressed in adding the ability to delete environments: ([gitlab#20620](https://gitlab.com/gitlab-org/gitlab/issues/20620) and [gitlab#19724](https://gitlab.com/gitlab-org/gitlab/issues/19724)).

## Top Vision Item(s)

An exciting focus for the vision is the ability to support advanced planning and execution of Releases. We will accomplish this with runbooks via [gitlab#9427](https://gitlab.com/gitlab-org/gitlab/issues/9427) while expanding the Releases page UI in [gitlab&2312](https://gitlab.com/groups/gitlab-org/-/epics/2312).

These features will enable both technical and non-technical users during the creation and management of releases-as-code within GitLab.  
