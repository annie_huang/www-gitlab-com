---
layout: markdown_page
title: "Using GitLab for ISO 26262-6:2018 - Product development at the software level"
---

## Understanding ISO 26262-6:2018

ISO 26262-6 aims to address the specific needs of electrical and/or electronic systems within road vehicles. The amount and complexity of technology in road vehicles is increasing, including the development and use of software. ISO 26262-6:2018 specifically prescribes requirements for product development at the software level for automotive applications to identify and mitigate software risks in road vehicles.

## Enforcement of coding guidelines and requirements ISO 26262-6:2018

ISO 26262-6:2018 prescribes several guidelines and requirements for product development at the software level, including:

* Enforcement of low complexity
* Enforcement of strong typing
* Use of well-trusted design principles
* Use of style guidelines
* Use of naming conventions
* Restricted size and complexity of software components
* Strong cohesion within each software component
* Loose coupling between software components

GitLab offers several tools that may help organizations with such requirements:

* [Merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html) can be used to enforce requirements and require review by specific persons or teams before merging the change. Beyond enforcing software requirements, this can be a powerful tool for document control change management for files stored in a project repository.

* [Push rules](https://docs.gitlab.com/ee/push_rules/push_rules.html) can be used to set requirements for commit messages. One example of how this can be used is to enforce a requirement that every commit must reference a requirement in Jira or the wiki.

* [Protected branches](https://docs.gitlab.com/ee/user/project/protected_branches.html) allow you to enforce rules for creating, pushing to, and deleting branches.

* In the future, the [Requirements Management](https://gitlab.com/groups/gitlab-org/-/epics/670) product category will allow for a more complete and structured way to granularly enforce and audit compliance with complex requirements.

## Managing documentation and standards

ISO 26262-6:2018 requires organizations create and manage documentation and standards for product development at the software level, including establishing design principles and defining requirements. GitLab has several built-in tools to help your organization manage the creation, modification, tracking, and distribution of documentation.

Examples of Documentation required by ISO 26262-6:2018 include:
* Software architectural Design
* Software safety requirements specifications
* Software unit design
* Defensive implementation techniques
* Design principles
* Style guides

The GitLab [Wiki](https://docs.gitlab.com/ee/user/project/wiki/) is a documentation system that's built into each GitLab project. Wiki pages can be created and managed through an easy-to-use web interface or Git for more advanced users. A complete [history](https://docs.gitlab.com/ee/user/project/wiki/#viewing-the-history-of-a-wiki-page) of all wiki pages is maintained so a comprehensive record can be kept. The wiki can be used to detail project objectives, describe requirements, store design and architecture documentation, and more.

Documents can be stored in project [repositories](https://docs.gitlab.com/ee/user/project/repository/) and any changes to them tracked using [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/). Repositories can be used to store any file type, including PDFs, spreadsheets, Word documents, and more. A common choice is to use Markdown files for documentation, which make it easy to modify and track changes to the files directly from GitLab.

With GitLab offering several [integrations](https://docs.gitlab.com/ee/user/project/integrations/index.html), including [Jira](https://docs.gitlab.com/ee/user/project/integrations/jira.html), it's easy to extend GitLab's PPM features.

[Project import and export](https://docs.gitlab.com/ee/user/project/settings/import_export.html) can also be used to efficiently create project-level templates, making it easy to create and replicate more complex workflows across several projects in GitLab. Project exports include everything in the project, including issues and files, making it a powerful tool for seamlessly replicating sophisticated workflows with little effort.

## Software unit design, implementation, and verification

With [description templates](https://docs.gitlab.com/ee/user/project/description_templates.html), you can create templates with the requirements and other considerations the development team need to incorporate into the software or development process. Along with [task lists](https://docs.gitlab.com/ee/user/markdown.html#task-lists), you can track the incorporation of requirements into the process and provide an easy-to-use way to track those tasks for developers and reviewers.

To enforce requirements and coding standards throughout the software development lifecycle, you can use [merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html) to require all merge requests are reviewed and approved by the appropriate person(s). With [push rules](https://docs.gitlab.com/ee/push_rules/push_rules.html), you can create and enforce unique rules which, if not followed, will decline a push. And using [protected branches](https://docs.gitlab.com/ee/user/project/protected_branches.html), you can set granular permissions for who can interact with branches and how, further enforcing specific requirements. In the future, the [Requirements Management](https://gitlab.com/groups/gitlab-org/-/epics/670) product category will allow for a more complete and structured way to granularly enforce and audit compliance with complex requirements.

Using [Service Desk](https://about.gitlab.com/product/service-desk/), you can interact with and collect feedback from customers and other external stakeholders. This allows you to seamlessly monitor, document, and evaluate feedback as part of your software maintenance process, straight from GitLab.

Using [SAST](https://docs.gitlab.com/ee/user/application_security/sast/), you can identify known vulnerabilities in your source code. Setting up SAST is easy - you can either include the [SAST CI job](https://docs.gitlab.com/ee/user/application_security/sast/) or use [Auto SAST](https://docs.gitlab.com/ee/topics/autodevops/#auto-sast-ultimate). After that’s done, every time the job is run, your source code will be scanned. When the scan is done, the results are [displayed](https://docs.gitlab.com/ee/user/application_security/sast/#how-it-works) right on the merge request. And when you go to any pipeline with a SAST job, you’ll be shown a [Security Report](https://docs.gitlab.com/ee/user/application_security/sast/#security-report-under-pipelines) with the findings.

## Disclaimer

THE INFORMATION PROVIDED ON THIS WEBSITE IS TO BE USED FOR INFORMATIONAL PURPOSES ONLY. THE INFORMATION SHOULD NOT BE RELIED UPON OR CONSTRUED AS LEGAL OR COMPLIANCE ADVICE OR OPINIONS. THE INFORMATION IS NOT COMPREHENSIVE AND WILL NOT GUARANTEE COMPLIANCE WITH ANY REGULATION OR INDUSTRY STANDARD. YOU MUST NOT RELY ON THE INFORMATION FOUND ON THIS WEBSITE AS AN ALTERNATIVE TO SEEKING PROFESSIONAL ADVICE FROM YOUR ATTORNEY AND/OR COMPLIANCE PROFESSIONAL.