---
layout: markdown_page
title: "Diversity and Inclusion Advisory Group Guidelines"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction
This document provides GitLabbers with the information they need to understand and sustain a Diversity & Inclusion Advisory Group here at GitLab. In this guide, you will:


*   Introduction to Diversity and Inclusion Groups 
*   The definition of Diversity and Inclusion
*   What is a diversity and inclusion group
*   The purpose of the global diversity and inclusion council


### Purpose and Benefits of a D&I Advisory Group

Internal diversity groups are a valuable component of a company’s diversity and inclusion strategy, because they provide “an inclusive and effective mechanism for managing and initiating diversity and inclusion and D&I programs.  Diversity councils/groups successfully integrate the companies’ D&I programs with their operations, strategies, and missions and objectives. They provide platforms for assessing the effectiveness of the D&I program, introducing reform, and overseeing the D&I.  They also demonstrate the companies’ commitment to D&I. 

The Global D&I Advisory Group is designed to provide greater representation of the diversity and inclusion of GitLabs workforce.  The group will assist with global feedback in implementing the diversity and inclusion strategy, policies and initiatives. 

[### DEFINITION OF DIVERSITY AND INCLUSION](https://about.gitlab.com/company/culture/inclusion/)

### Responsibilities of the Group
*   Align GitLabs organization and programs with business needs, practices, and strategies
*   Have a long-term strategy, stated purposes, goals, and objectives, and a mission statement 
*   Work closely with executive and operational leadership, diversity and people ops leadership, staff, and organizations (especially employee resource groups) 
*   Partner with senior leadership for support and participation 
*   Demonstrate the importance of diversity to the success of the enterprise 
*   Communicate the goals and objectives, program efforts, and successes consistently and repeatedly to GitLab and its workforce 
*   Secure a reputation as a source of good counsel, support, and understanding
*   Each member should commit to serving a minimum of 1 year in the group

### Roles Within the Group

#### Advisory Group Members 

At GitLab we all contribute!  Everyone has an opportunity to lead and provide feedback within the group.

#### Executive Sponsor  
An executive GitLab team member who is responsible and accountable for strategic support of the group

*   Share accountability for the success of the D&I group
*   Participate as an active member of the D&I group
*   Share information about the D&I group activities with other leaders
*   Provide insight and guidance to D&I group as needed
*   Partner with the D&I group/council Lead and Co-Lead on issues, concerns, and resource needs of the community 
*   May provide additional budget

#### Lead
A GitLab team member who is responsible and accountable for strategic direction and operations of the Global D&I Advisory Group 

*   Operational lead of the Global D&I Advisory Group
*   Meets w/ D&I Partner regularly
*   Responsible for submitting annual and quarterly plans 
*   Along with Vice-chair, serves as contact for any team, department, or other GitLabber requesting partnership or education with the Global D&I Advisory Group 


#### Co-Lead
A GitLab team member who supports the Lead and Sponsor in the strategy and operations of the Global D&I Advisory Group

*   Serve as lead in the absence of the Chair
*   Along with Chair, serves as contact for any team, department, or other GitLabber requesting partnership with the D&I group

