---
layout: markdown_page
title: "Building an Inclusive Remote Culture"
---

## On this page
{:.no_toc}

- TOC
{:toc}

#### Introduction

We are passionate about [all remote working](https://about.gitlab.com/company/culture/all-remote/vision/) and enabling an [inclusive work](https://about.gitlab.com/company/culture/inclusion/#fully-distributed-and-completely-connected) environment. There isn't one big activity we can take to accomplish this.  Instead it is a mix of numerous activities / behaviours combined to enable our team members feel they belong in GitLab.

Those activities / behaviors include: 

* [D&I Events](https://about.gitlab.com/company/culture/inclusion/diversity-and-inclusion-events/)
* [D&I Advisory Group](https://about.gitlab.com/company/culture/inclusion/#understanding-of-the-purpose-of-the-global-diversity--inclusion-advisory-group)
* [Diversity & Inclusion ERGs - Employee Resource Groups](https://about.gitlab.com/company/culture/inclusion/#ergs---employee-resource-groups) 
* [Our Values](https://about.gitlab.com/handbook/values/)
* [Family and Friends first, Work second](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second)
* [Inclusive language and pronouns](https://about.gitlab.com/handbook/values/#inclusive-language--pronouns)
* [Parental leave](https://gitlab.com/gitlab-com/people-group/Compensation/issues/46#note_289805708) 
* [Async Communication](https://about.gitlab.com/handbook/values/#bias-towards-asynchronous-communication)

#### Tips for People Managers 


## Tips for Companies  
1. **Defining Diversity & Inclusion** A great place to begin is to set a foundation of the basic understanding of how your company defines these terms.  Most places you will hear them used interchangeably.  Understanding they are different is essential to driving the initiatives.  As an example, you can hire as many diverse candidates but if you don't create an inclusive environment the work can be in vain.  
1. **Evaluating the companies current D&I landscape** Consider what you are already doing in this space?  What is the current feedback? What are team members saying with company engagement surveys?  What are the goals you are wanting to achieve?  What are the metrics saying?
1. **Naming this body of work** Although Diversity & Inclusion are often understood globally there are other terms that can be leveraged to name your efforts.  The naming should be unique to your company.  Examples could include (i.e, Belonging, Inclusion & Collaborations, etc.)
1. **Developing a mission statement** When creating a diverse and inclusive culture, most companies will develop a mission statement to support their vision.  Your mission statement should articulate the purpose of your strategy.  In a few sentences you should be able to succinctly provide the why and the how. Be sure to take into account your company’s current overall mission and vision. It is best to align your D&I mission and vision with your organization’s overarching mission and vision. To do this, you may consider how your D&I Strategy can build on, scale, and or enhance the organization’s mission and vision.
1. **Creating ERGs** In general ERGs are an excellent support system and key to providing awareness, respect, and building diversity and inclusion within the workplace. These groups are a proven way to increase cultural competency, retention of team members, provide marketplace insights to the business, attract diverse talent, and more. The endorsement of ERGs gives team members the opportunity to identify common interests, and decide how they can be shared with others.  When creating ERGs there are few initial steps to consider:
* [Creating guidelines to help support ERGs being stood up within your company](https://about.gitlab.com/company/culture/inclusion/erg-guide/)
    * Naming of each ERG
    * Roles within each ERG
    * Aligning to company strategy
* Creating forms, google groups ways of tracking attendance for ERG events and membership metrics
1. More to come - D&I Advisory Group / D&I Initiatives / D&I Awards for Recognition / D&I Surveys / D&I Framework Inclusive Benefits?

## Tips for Managers

1. **Set aside time to show up for planned D&I events.** You might be surprised by how much seeing your face in these events validates to others that this is a worthwile use of time that is valued by the company. Seeing you in attendance also opens to the door to future conversations that might begin "I saw you in XYZ meeting, what did you think of such-and-such topic?"
1. **Incorporate D&I into your team meetings** Being able to connect to your team members is key in understanding who they are and what they bring to the team.  This can be done in several ways but a great initial step is to start with open discussions. You could start by opening your next team meeting to chat about what they feel inclusion looks like, what is important to them as a team to feel included, etc.  This could then move into monthly or quarterly team D&I icebreakers, trainings etc.  The goal is to make sure D&I is not touched on once and never mentioned again but more of an understood aspect of your team environment.  
1. **Ask employees what pronouns they use** Pronouns are a large piece of a person’s identity and are often used to communicate a person’s gender, which is why it is so important to get it right.  Asking for a person’s pronouns and using those pronouns consistently shows that you respect their identity, but it also helps to create a more welcoming, safe and supportive environment where people can feel comfortable to be themselves.  This is a change that goes a long way to foster inclusion.
1. **Be mindful of the times of your meetings** This may already be understood however we can sometimes forget to consider global times and alternating meetings to accomodate regions, team members with families or times chosen may be out of their working hours, etc.  Every meeting time won't be perfect for everyone but making a conscious effort to alternate times is to ensure the same people aren't being excluded.

