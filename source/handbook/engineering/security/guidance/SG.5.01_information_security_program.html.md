---
layout: handbook-page-toc
title: "SG.5.01 - Information Security Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SG.5.01 - Information Security Program

## Control Statement

GitLab has an established security leadership team including key stakeholders in GitLab's Information Security Program; goals and milestones for deployment of the information security program are established and communicated to the company.

## Context

This control requires that GitLab has and maintains an established security leadership team which is charged with the overall governance of GitLab's Information Security Program. The establishment of leadership is key to helping to shape and define the overall information security program goals and objectives. Additionally, these goals and objectives should be communicated to the company as a whole where applicable as all GitLabbers are required to adhere to any policies that are created as part of the overall information security program.

## Scope

The scope of this control is GitLab as a company and the control has been put in place to ensure that GitLab continues to keep security leadership defined and that all information security program goals and milestones are established/documented. Policies created as a result of the goals and milestones of the informaiton security program are communicated to the company as required.

## Ownership

* Control Owner: `Security Team`
* Process owner(s):
    * Security Team: `100%`

## Guidance

This control is operational in nature as it is performed as part of regular internal business processes and procedures. The [GitLab Org Chart](https://about.gitlab.com/company/team/org-chart/) is kept up-to-date and serves as the establishement of security leadership. rogram goals and milestones are tracked through Objective and Key Results (OKRs) issue boards that are published to the [Security](https://about.gitlab.com/handbook/engineering/security/#how-to-track-okr-related-work-scheduled-by-quarter) handbook page. Communication to the company is established via announcements made on the Company Call and/or via the `#company-announcements` Slack channel as required. The OKRs are publicly available for anyone to access.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Information Security Program control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/883).

### Policy Reference
*  [Security OKR Tracking](https://about.gitlab.com/handbook/engineering/security/#how-to-track-okr-related-work-scheduled-by-quarter)