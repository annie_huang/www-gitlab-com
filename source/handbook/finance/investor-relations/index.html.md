--- 
layout: handbook-page-toc
title: "Investor Relations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Earnings Release Calendar (workback schedule)

Task/Item - Key Contributors

### 5 Weeks Prior - Kick Off & Debrief	
* First check-in: debrief and develop key themes for stockholder letter - IR/CEO/CFO/CLO

### 4 Weeks Prior	
* Press release: date of earnings release, access instructions - IR
* Overview of prelim financials and key metrics, CFO topics	- IR/CFO/PAO/VP FP&A
* Audit process begins, preliminary financials available - PAO
* Business update & overview of department initiatives (1:1s) - IR/CMO/CRO/EVP Product

### 3 Weeks Prior	
* Stockholder letter, v1 - IR/CEO/CFO/CLO
* Opening remarks, v1	- IR/CEO/CFO/CLO
* Q&A document, v1 - IR/PAO/VP FP&A

### 2 Weeks Prior	
* Stockholder letter, v2 - IR/CEO/CFO/CLO
* Opening remarks, v2 - IR/CEO/CFO/CLO
* Q&A document, v2 - IR/CFO/PAO
* Earnings press release, v1 - IR/CFO/CLO
* Update investor presentation - IR/Accounting/FP&A
* Submit documents for external legal review - CLO

### 1 Week Prior	
* Stockholder letter, vF - IR/CEO/CFO/CLO/VP, FP&A
* Opening remarks, vF - IR/CEO/CFO/CLO
* Q&A document, vF and simulated Q&A first session - IR/CEO/CFO/CLO
* Audit committee approval: earnings release, 10-Q/K - PAO/audit committee
* Disclosure committee review all docs and address open comments/issues - disclosure committee

### 3 Days Prior	
* Lock Documents: stockholder letter, opening remarks, earnings release	
* Share stockholder letter with audit committee and communications  
* Read-through opening remarks, simulated Q&A second session - IR/CEO/CFO/CLO/VP of FP&A

### 1 Day Prior	
* Final rehearsal of talking points, simulated Q&A third session - IR/CEO/CFO/CLO/VP, FP&A

### Day of Earnings	
* Final Q&A Prep - IR/CEO/CFO/VP FP&A/CLO
* Earning release proof	- Finance, Accounting
* Shareholder Letter (tables/talking points) - IR/CEO/Finance/CLO
* NYSE / Nasdaq notification - IR
* Release hits wire	- IR
* 8-k filed with SEC - CLO
* Host earnings call - IR/CEO/CFO
* Analyst callbacks	- IR/CEO/CFO/CLO/VP, FP&A

### Day after Earnings	
* Team member company update - GitLab/Internal comms
* File 10-Q
* Buy-side callbacks - IR/CEO/CFO

### Continuous updates: abridged version of financials and key operating metrics, consensus, and Q&A tracker 

*  Attendees: CEO, CFO, CLO, and IR
*  Discussion topic: Identify key developments from the prior quarter for inclusion in the shareholder letter

Day 10: Begin external audit process - DRI is PAO

Day 12: Preliminary financial results and key metrics

*  Attendees: CFO, PAO, VP FP&A, and IR
*  Discussion topic: Drivers of financial and operating metric overperformance and underperformance to expectations

Day 22: Submit press release and shareholder letter for legal review - DRI is IR

Day 25: Submit press release and quarterly filing to Audit Committee - DRI is PAO

Day 30: [Disclosure Committee](/handbook/internal-audit/sarbanes-oxley/#disclosure-committee-charter) meeting

*  Attendees: CLO, PAO, VP of FP&A, and IR
*  Discussion topics: Approve shareholder letter, earnings release including final financial tables, investor presentation, and quarterly or annual SEC filing

We anticipate that our quarterly trading window will open the third trading day after the announcement of our quarterly results and that it will close again immediately prior to the last four weeks of the fiscal quarter, as indicated by an announcement made by the CLO. However, it is important to note that any quarterly trading window may be cut short if the CLO determines that material nonpublic information exists in such a fashion that it would make trades by directors, employees, and consultants inappropriate.

## Performance Indicators (assuming publicly traded)

On a rolling 12-month basis, be among the top quartile of least volatile stocks compared to GitLab's public company peer set consisting of DevOps and software growth firms.

Greater than two-thirds of active, covering sell-side analysts describe the Company the same way we do: conclusions will vary.

Greater than 80% of questions during an earnings call have been anticipated.

### Enterprise Value to Sales
Enterprise Value to Sales compares the enterprise value (EV) of a company to its annual sales

Enterprise Value to Sales = Enterprise Value/Annual Sales

Enterprise Value = Market Capitalization + Debt - Cash and Cash Equivalents
