---
layout: handbook-page-toc
title: Approved Operating Systems
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

At GitLab it's vital we ensure the data of our Customers, our team-members, and our company is treated and protected with the appropriate security measures it deserves.

With this in mind, GitLab Security will begin enforcing a list of approved Operating Systems and minimum Operating System versions for use at GitLab. This change will decrease the risk from the use of End-Of-Life or unapproved Operating Systems accessing locations which may contain sensitive data.

Please see the [Data Classification Policy](/handbook/engineering/security/data-classification-policy.html) handbook page for further details on how different data types are classified.

## Approved Operating Systems

_last updated 2020-01-08_

### MacOS

MacOS 10.13.6 or later

### Linux & *BSD

Ubuntu 14.04 LTS or later is recommended by GitLab, however we only require you run an actively updated and supported Linux disto or BSD flavour. It will be your responsibility to maintain your Linux environment.

Further details are available at the [Linux Tools & Tips](https://about.gitlab.com/handbook/tools-and-tips/linux-tools/) page.

### Android

Android 7.1.2 or later

### iOS

iOS 10.3.4 or later

### Windows

As described in the [Acceptable Use Policy](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/#unable-to-use-company-laptop), Microsoft Windows operating systems are not allowed. If you have a legitimate business need to use a Windows operating system, please see the [Exception Process](#exception-process).

## Access Change Schedule

### January 20th, 2020

All **Windows** users must submit Exception Requests for the continued use at GitLab, and any unauthorized user of Windows will be notified. These users must have a legitimate business need which cannot be accomplished or is significantly more difficult on a MacOS or Linux OS, must use Windows 10 or later, and have manager approval.

### January 27th, 2020

Users of Android 7.1.1 or older will be notified and required to upgrade to a supported version of Android OS. Access restrictions may be enforced if use of unauthorized versions of Android persist.

## Exception Process

Exception requests may be submitted to the [Security Compliance Team](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues) using the [Exception template](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/.gitlab/issue_templates/Exception%20Request.md). Security Compliance and Security Operations will review requests as they come in.

Further information about the Exception Management process is available in the [GitLab Handbook](https://about.gitlab.com/handbook/engineering/security/#information-security-policy-exception-management-process)
