---
layout: handbook-page-toc
title: "Global Volunteer Month"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Global Volunteer Month
This initiative is aimed at volunteering during the month of December each year, within your local community. 

1. Join `#GiveLab` in Slack by mid-November of that year and start searching for a local initiative you can support.

1. This initiative includes any form of volunteering! Some examples to help you search:
    - Volunteering to create a quick and easy website for a soup kitchen.
    - Helping a non-profit fix bugs in their app.
    - Collecting trash at your local park.
    - Serving at a soup kitchen.
    - Putting together a small box of things for kids in an underprivileged community.
    - Reading at a local children's home or shelter.

1. This can be done on work time, but notify your manager in advance if so.

1. If you'd like to share your volunteering on social media, please tag @gitlab and use the tag: #GiveLab\
*GitLab social handles may engage with and/or share your post.*\
*The ability to share posts is dependent on their demands at the time and is not promised.*

1. Thank you for taking part and helping us spread some love!
