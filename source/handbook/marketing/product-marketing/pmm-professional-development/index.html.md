---
layout: handbook-page-toc
title: "Product Marketing Professional Development"
---


## Topics/Resources  (sections with suggested resources/links)
Below are a list of resources and links to suggested professional development tools and resources:

### Domain Specific Resources (SDLC/DevOps)

1. Agile (scrum,kanban, etc)
   1. **Videos and Webpages**
      - [GitLab Agile Delivery](https://about.gitlab.com/solutions/agile-delivery/)
      - [Scrum.org- What is Scrum](https://www.scrum.org/resources/what-is-scrum)
      - [What is Kanban](https://www.digite.com/kanban/what-is-kanban/)
   1. **Courses:**

   1. **Conferences:**
   
1. DevOps
   1. **Videos and Webpages**
      -[GitLab DevOps](https://about.gitlab.com/devops/)
       (Specifically the resources list at the bottom)
   1. **Courses:**

   1. **Conferences:**

1. Software Programming


### Product Marketing specific resources
1. Product Marketing


1. Public Speaking 


1. Storytelling


### Marketing in general
